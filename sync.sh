if [ -d "build" ]; then
    GIT_COMMIT=$(git rev-parse HEAD)
    GIT_USER=$(git config user.name)
    REPO_VERSION=$(npm pkg get version)
    echo $GIT_USER > build/signatures.txt
    echo $GIT_COMMIT >> build/signatures.txt
    echo $REPO_VERSION >> build/signatures.txt

    URI="nil"
    if [ "$1" == "qa" ]; then
        URI="gs://qa-redirect.blockpass.org"
    elif [ "$1" == "staging" ]; then
        URI="gs://sandbox-redirect.blockpass.org"
    elif [ "$1" == "prod" ]; then
        URI="gs://redirect.blockpass.org"
    fi

    echo Now sync data to $URI
    if [ "$URI" == "nil" ]; then
        echo USAGE: sh sync [env]
    else
        gsutil rsync -r build $URI
        gsutil setmeta -h "Content-Type:application/json" $URI/.well-known/apple-app-site-association
        gsutil setmeta -h "Cache-Control:no-cache" $URI/index.html
    fi
else
    echo "Missing build folder"
fi
