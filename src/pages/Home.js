import "../App.css";
import logo from "../logo.png";
import {
  BrowserView,
  MobileView,
  isAndroid,
  isIOS,
  isMobile,
} from "react-device-detect";
import React, { useEffect, useRef, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import Colors from "@utils/Colors";

const Home = () => {
  const location = useLocation();
  const appStoreButtonRef = useRef(null);
  const timeRef = useRef(null);
  const interRef = useRef(null);
  const isFocusRef = useRef(true);
  const [logs, setLogs] = useState([]);
  const searchParams = new URLSearchParams(location.search);
  const schema = searchParams.get("schema") || "blockpass://";
  console.log("schema", schema);
  const isi = searchParams.get("isi") || "1592260586";
  const apn = searchParams.get("apn") || "org.blockpass.mobile";

  useEffect(() => {
    autoOpenAppStore();
  }, []);

  const autoOpenAppStore = () => {
    if (appStoreButtonRef.current && isFocusRef.current) {
      setLogs((state) => [...state, "autoOpenAppStore"]);
      appStoreButtonRef.current.click();
    }
  };

  const openAppStore = () => {
    const appStoreUrl = getAppStoreUri();
    window.location.href = appStoreUrl;
  };

  const getAppStoreUri = () => {
    if (isAndroid)
      return `https://play.google.com/store/apps/details?id=${apn}`;
    const appStoreUrl = `https://apps.apple.com/app/id${isi}`; //?mt=8&at=${redirectUrl}
    if (isIOS) return appStoreUrl;
  };

  const openBlockpassApp = () => {
    if (isMobile) {
      // Try to open the app URL scheme
      window.location.href = schema;
      timeRef.current = new Date();
      setLogs((state) => [...state, "openBlockpassApp"]);
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <div>
          <img src={logo} alt="logo" style={styles.img} />
          {/* <BrowserView>
            <h3>
              Browser is detected! Dynamic Link is not supported on Desktop,
              please open this link on mobile{" "}
            </h3>
          </BrowserView> */}
          <MobileView>
            {/* {isAndroid && <h5>Android device is detected!</h5>}
            {isIOS && <h5>iOS device is detected!</h5>} */}
            {/* <div onClick={() => openBlockpassApp()} style={styles.btn}>
              Open Blockpass App
            </div> */}
            <div
              ref={appStoreButtonRef}
              onClick={() => openAppStore()}
              style={styles.btn}
            >
              Go to AppStore
            </div>
          </MobileView>
          {/* {logs.map((log, idx) => (
            <div key={idx}>
              <span>{log}</span>
              <br />
            </div>
          ))} */}
        </div>
      </header>
    </div>
  );
};

export default Home;

const styles = {
  img: {
    height: 150,
  },
  btn: {
    backgroundColor: Colors.primary,
    color: "white",
    marginLeft: 32,
    marginRight: 32,
    paddingTop: 8,
    paddingBottom: 8,
    marginTop: 32,
    paddingLeft: 12,
    paddingRight: 12,
  },
};
