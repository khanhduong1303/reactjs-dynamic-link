import "../App.css";
import logo from "../logo.png";
import { BrowserView, isAndroid } from "react-device-detect";
import React, { useEffect, useState } from "react";
import { useLocation, useHistory } from "react-router-dom";
import Colors from "@utils/Colors";
import { DownOutlined } from "@ant-design/icons";
import { Radio, Input, Button, Typography, Space, Dropdown } from "antd";
import { buildBPLink } from "@blockpass-org/utils-dynamic-link";
const { Text, Link } = Typography;

const SCHEMAS = {
  prod: "blockpass://",
  qa: "blockpass-qa://",
  staging: "blockpass-staging://",
};

const APPID = {
  ios: "1592260586",
  android: "org.blockpass.mobile",
};

const DOMAINS = {
  prod: "applink",
  qa: "qa-applink",
  staging: "staging-applink",
};

const ACTIONS = [
  {
    label: "Open App",
    key: "home",
  },
  {
    label: "Forgot password",
    key: "forgot-password",
  },
  {
    label: "Register service",
    key: "service-register",
  },
  {
    label: "WID migration",
    key: "wid-migration",
  },
];

const PARAMS = {
  home: [],
  "forgot-password": ["email", "session"],
  "service-register": ["clientId", "refId", "session"],
  "wid-migration": ["email", "migrationId"],
};

const Setup = () => {
  const location = useLocation();
  const history = useHistory();
  const [env, setEnv] = useState("qa");
  const [platform, setPlatform] = useState(isAndroid ? "android" : "ios");
  const [action, setAction] = useState("home");
  const [schema, setSchema] = useState(`${SCHEMAS[env]}${action}`);
  const [appId, setAppId] = useState(APPID[platform]);
  const [params, setParams] = useState({});

  useEffect(() => {}, []);

  const onChange = (key, e) => {
    console.log("onChange", key, e?.target?.value || e?.key);
    const _val = e?.target?.value;
    if (key === "env") {
      setEnv(_val);
      if (_val === "custom") {
        setSchema("");
        setAppId("");
        return;
      }
      // setSchema(`${SCHEMAS[_val]}${action}${getQueries()}`);
      setSchema(getSchema(_val, action, { queries: params }));
      setAppId(APPID[platform]);
    }
    if (key === "platform") {
      setPlatform(_val);
      setAppId(APPID[_val]);
    }
    if (key === "schema") setSchema(_val);
    if (key === "appId") setAppId(_val);
    if (key === "action") {
      const _action = e.key;
      setAction(_action);
      // setSchema(`${SCHEMAS[env]}${_action}${getQueries()}`);
      setSchema(getSchema(env, _action, { queries: params }));
      setParams({});
    }
    if (key.includes("param_")) {
      const _params = {
        ...params,
        [key.replace("param_", "")]: _val,
      };
      setParams(_params);
      // setSchema(`${SCHEMAS[env]}${action}${getQueries(_params)}`);
      setSchema(getSchema(env, action, { queries: _params }));
    }
  };

  const onTestLink = () => {
    history.push(`/?appId=${appId}&schema=${schema}`);
  };

  const onCopyLink = async () => {
    try {
      await navigator.clipboard.writeText(getLink());
    } catch (err) {
      console.error("Failed to copy: ", err);
    }
  };

  const getQueries = (_params) => {
    const query =
      Object.keys(_params || params).length > 0
        ? `?${new URLSearchParams(_params || params).toString()}`
        : "";
    return query;
  };

  const getSchema = (e, a, q) => {
    const _dynamicLink = buildBPLink(e, a, q);
    console.log("kkkkk _dynamicLink param", e, a, q, _dynamicLink);
    if (_dynamicLink?.split("schema=").length > 0) {
      return _dynamicLink?.split("schema=")[1];
    } else {
      return schema;
    }
  };

  const getLink = () => {
    if (env === "custom") {
      const domain = window.location.hostname.split(".")[0];
      const paths = window.location.href.split("#");
      const newHost =
        domain !== "localhost"
          ? paths[0].replace(domain, DOMAINS[env])
          : paths[0];

      const link = `${newHost}?appId=${appId}&schema=${schema}`;
      return link;
    }

    return buildBPLink(env, action, { queries: params });
  };

  return (
    <div style={styles.container}>
      <div style={styles.wrap}>
        <img src={logo} alt="logo" style={styles.img} />
        <h2 style={{ color: "white" }}>Setup Dynamic Link</h2>
        <Radio.Group onChange={(e) => onChange("env", e)} value={env}>
          <Radio value={"qa"} style={styles.radio}>
            QA
          </Radio>
          <Radio value={"staging"} style={styles.radio}>
            Staging
          </Radio>
          <Radio value={"prod"} style={styles.radio}>
            Prod
          </Radio>
          {/* <Radio value={"custom"} style={styles.radio}>
            Custom
          </Radio> */}
        </Radio.Group>
        {/* <BrowserView>
          <h5 style={{ color: "white" }}>
            You are using Browser, please select platform
          </h5>
          <Radio.Group
            onChange={(e) => onChange("platform", e)}
            value={platform}
          >
            <Radio value={"ios"} style={styles.radio}>
              iOS
            </Radio>
            <Radio value={"android"} style={styles.radio}>
              Android
            </Radio>
          </Radio.Group>
        </BrowserView> */}
        {env !== "custom" && (
          <div style={{ marginTop: 8 }}>
            <Dropdown
              menu={{
                items: ACTIONS,
                onClick: (e) => onChange("action", e),
              }}
              trigger={["click"]}
            >
              <a onClick={(e) => e.preventDefault()}>
                <span
                  style={{
                    color: "white",
                  }}
                >
                  Action type: &nbsp;&nbsp;
                  <Space style={{ color: Colors.primary, cursor: "pointer" }}>
                    {ACTIONS.find((_) => _.key === action)?.label}
                    <DownOutlined />
                  </Space>
                </span>
              </a>
            </Dropdown>
          </div>
        )}
        {env !== "custom" && PARAMS[action].length > 0 && (
          <div style={{ ...styles.inputGroup, ...styles.params }}>
            <span
              style={{
                color: "white",
                textAlign: "left",
              }}
            >
              Parameters
            </span>
            {PARAMS[action].map((key, idx) => (
              <Input
                key={idx}
                placeholder={key}
                style={{
                  ...styles.input,
                }}
                value={params[key]}
                onChange={(txt) => onChange(`param_${key}`, txt)}
              />
            ))}
          </div>
        )}

        <div style={styles.inputGroup}>
          <Input
            placeholder="example://"
            style={{
              ...styles.input,
              color: env !== "custom" ? "white" : null,
            }}
            disabled={env !== "custom"}
            value={schema}
            onChange={(txt) => onChange("schema", txt)}
          />
          <Input
            placeholder={`AppId(ex: ${
              platform === "ios" ? "1234567890" : "com.dynamiclink.mobile"
            })`}
            style={{
              ...styles.input,
              color: env !== "custom" ? "white" : null,
            }}
            disabled={env !== "custom"}
            value={appId}
            onChange={(txt) => onChange("appId", txt)}
          />
        </div>
        <div>
          <Text style={{ color: "white" }}>{getLink()}</Text>
        </div>
        {/* <Button
          type="primary"
          style={{ ...styles.btn, color: !schema || !appId ? "white" : null }}
          onClick={onTestLink}
          disabled={!schema || !appId}
        >
          Test link
        </Button> */}
        <Button
          type="primary"
          style={{
            ...styles.btn,
            color: !schema || !appId ? "white" : null,
            marginLeft: 12,
          }}
          onClick={onCopyLink}
          disabled={!schema || !appId}
        >
          Copy link
        </Button>
        <div>
          <Text type="danger">
            Note: To make sure it works correctly. Please do not paste this link
            directly into the browser, instead forward this link via email and
            click on the link from GMail or Ansana or in the Notes except paste
            it directly in the browser!
          </Text>
        </div>
      </div>
    </div>
  );
};

export default Setup;

const styles = {
  container: {
    display: "flex",
    height: "100vh",
    backgroundColor: Colors.backgroundColor,
    justifyContent: "center",
    alignItems: "center",
  },
  wrap: {
    maxWidth: 700,
    paddingLeft: 12,
    paddingRight: 12,
  },
  img: {
    height: 150,
  },
  radio: {
    color: "white",
  },
  inputGroup: {
    marginTop: 12,
  },
  params: {
    paddingLeft: 12,
    paddingRight: 12,
  },
  input: {
    marginTop: 4,
  },
  btn: {
    marginTop: 12,
  },
};
