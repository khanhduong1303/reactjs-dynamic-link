import { Home, Setup } from "@pages/index";
import { Switch, Route, HashRouter } from "react-router-dom";

import { Result } from "antd";

function App() {
  const isProd = window.location.origin.includes(
    "https://redirect.blockpass.org"
  );

  return (
    <div className="App">
      <HashRouter basename="/">
        <Switch>
          <Route exact path="/" component={() => <Home />} />
          {!isProd && <Route exact path="/setup" component={() => <Setup />} />}
          <Route
            component={() => (
              <Result
                status="404"
                title="404"
                subTitle="Sorry, the page you visited does not exist."
              />
            )}
          />
        </Switch>
      </HashRouter>
    </div>
  );
}

export default App;
