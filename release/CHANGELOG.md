# CHANGELOG

## 0.1.2 - 2023/10/11

- Fix missing Android SHA256 for Prod build
- Git hash: e967fb9f8048c4443d2febfaa51d63a2d96a2664

## 0.1.1 - 2023/10/11

- First commit: 75246bc1852e75b089043a54aae90c0e1d8d0dbe
