const {
  override,
  useBabelRc,
  addWebpackModuleRule,
  addLessLoader,
} = require("customize-cra");
const path = require("path");
// eslint-disable-next-line react-hooks/rules-of-hooks
module.exports = override(
  useBabelRc()
  // addLessLoader({
  //   lessOptions: {
  //     modifyVars: {
  //       "@primary-color": "#F5222D", // Custom primary color
  //     },
  //     javascriptEnabled: true,
  //   },
  // })
);
